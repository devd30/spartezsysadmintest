import argparse
import requests
import json
from collections import Counter



parser = argparse.ArgumentParser(description='Get details of Team and Limit and Top argument for number of results')
parser.add_argument('team_name', nargs='+', type=str, help='Please enter the team name' )
parser.add_argument('top_committers', nargs='+', type=int, help='Please enter the number of top committers' )
parser.add_argument('committers_percentage', nargs='+', type=int, help='Limit the number of committers' )

args = parser.parse_args()
# print (args.team_name)
# print ((args.committers_percentage)[0])
# print ((args.top_committers)[0])

base_url = 'https://bitbucket.org/api/2.0/repositories'
url_api = '{base_url}/{team_name}/?q=scm="git"&pagelen=100'.format(base_url=base_url, team_name=args.team_name[0])
repos_name=[]

r = requests.get(url_api)
req_response = r.json()

for repo in (req_response['values']):
	repos=repo['slug']
	repos_name.append(repos)

#print (repos_name)

name_users=set()
commits_no = ((args.committers_percentage)[0])
commit_calculator = commits_no - 100


repo_det = {}

for repo in repos_name:
	#print ("current_repo-->",repo)
	username = {}
	r=requests.get("{base_url}/{team_name}/{repo_name}/commits?pagelen={commits_no}".format(base_url=base_url, team_name=args.team_name[0], repo_name=repo, commits_no=((args.committers_percentage)[0])))
	response = r.json()['values']
	try:
		for vals in response:
			#user = (vals['author']['raw'])
			user = (vals['author']['user']['username'])
			if user in username:
				username[user] += 1
			else:
				username[user] = 1
			repo_det[repo] = username
	except KeyError as e:
		continue

#print (repo_det)

top_commits={}

for outer_key,outer_val in repo_det.items():
	c=Counter(outer_val)
	most=c.most_common((args.top_committers)[0])
	top_commits[outer_key]=[most]



print (top_commits)